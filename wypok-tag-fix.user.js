// ==UserScript==
// @name        Wypok tag fix
// @description Fixes blocked endless scrolling for tag content
// @namespace globalbus
// @match     http://www.wykop.pl/tag/*
// @run-at document-end
// @grant none
// @version     1
// ==/UserScript==

//made by globalbus - Jan Filipski
var inject = document.createElement("script");

inject.setAttribute("type", "text/javascript");
inject.appendChild(document.createTextNode("(" + embed + ")()"));

document.body.appendChild(inject);
//trick for quit greasemonkey sandbox
//http://stackoverflow.com/questions/3542609/replacing-page-functions-via-userscript-in-chrome
function embed(){
    var marginDate =new Date("2005-12");
    var last = $('#itemsStream > li:last > div');
    if(last[0]===undefined)
        var date = new Date();
    else
        var date = new Date(last.find('time').attr('datetime'));
    var paginationButton = $('#ajaxPagination a');
    initialFix();

    function initialFix(){
        var ajaxUrl=paginationButton.data('ajaxurl');
        if(ajaxUrl.match('wszystkie')){
            date.addMonths(-1);
            ajaxUrl=ajaxUrl.replace('wszystkie','archiwum/'+date.getFullYear()+"-"+pad(date.getMonth()+1)+"/next/link-0");
            paginationButton[0].href=paginationButton[0].href.replace('wszystkie','archiwum/'+date.getFullYear()+"-"+pad(date.getMonth()+1)+"/next/link-0");
        }
        ajaxUrl=cutLastSlash(ajaxUrl);
        paginationButton[0].href=cutLastSlash(paginationButton[0].href);
        paginationButton.data('ajaxurl', ajaxUrl);
        //helper
        paginationButton.attr('data-ajaxurl',paginationButton.data('ajaxurl'));
        console.log(paginationButton);
    }

    function pad(n) {
        return (n < 10) ? ("0" + n) : n;
    }

    wykop.handleAjaxPagination = function ($el, html) {
        $items = $(html).find('li.iC');
        if ($items.length === 0) {
            paginationReplacer($el);
            $el.trigger('click');
            throw 'głupi wypok';
        } else {
            $('#itemsStream').append($items);
            wykop.bindSurveyForm($items);
            $last = $('#itemsStream > li:last > div');
            $el.data('ajaxurl', $el.data('ajaxurl').replace(/next\/(link-|entry-)?[0-9]+/, 'next/' + $last.data('type') + '-' + $last.data('id')));
            //helper
            $el.attr('data-ajaxurl',$el.data('ajaxurl'));
            $el.attr('href', $el.attr('href').replace(/next\/(link-|entry-)?[0-9]+/, 'next/' + $last.data('type') + '-' + $last.data('id')));
            if (typeof (ga) != "undefined") {
                ga('send', 'pageview', $el.attr('href').replace(/next\/(link-|entry-)?[0-9]+/, 'strona/' + (++wykop.loadedPage)));
            }
            $el.show();
            wykop.height.document = $(document).height();
            wykop.bindLazy();
            $('#paginationLoader').hide();
            wykop.nextPageLoading = false;
        }
    };
    function paginationReplacer($el){
        newDate=new Date(date);
        newDate.addMonths(-1);
        if(marginDate>newDate){
            wykop.nextPageLoading=true;
            $('#paginationLoader').hide();
            $('#ajaxPagination').hide();
            throw 'nie ma takiego scrollowania!';
        }
        var ajaxUrl=$el.data('ajaxurl');
        ajaxUrl=ajaxUrl.replace(date.getFullYear()+"-"+pad(date.getMonth()+1),newDate.getFullYear()+"-"+pad(newDate.getMonth()+1));
        $el[0].href=$el[0].href.replace(date.getFullYear()+"-"+pad(date.getMonth()+1),newDate.getFullYear()+"-"+pad(newDate.getMonth()+1));
        $el.data('ajaxurl', ajaxUrl);
        //helper
        $el.attr('data-ajaxurl',$el.data('ajaxurl'));
        date=newDate;
    }
    function cutLastSlash($url){
        if($url[$url.length-1]=="/")
            $url=$url.substring(0,$url.length-1);
        return $url;
    }
    wykop._ajaxCall = function($this, url, data, method, flag, event) {
        var data = data || {};
        var method = method || "GET";
        var flag = flag || "";
        var isForm = $this.is('form');
        if (flag !== "") {
            wykop.setFlag(flag);
        }
        if (!url.match(/hash\//)) {
            if (url.match(/\?/)) {
                url = url.replace("?", '/hash/' + wykop.params.hash + "?");
            } else {
                url += '/hash/' + wykop.params.hash;
            }
        }
        if (isForm) {
            $this.find(".submit i").show();
            $this.find(".submit").attr("disabled", "disabled");
        }
        $.ajax(url, {
            timeout: 1000,
            cache: false,
            error: function(jqXHR, textStatus, errorThrown) {},
            data: data,
            type: method,
            dataType: 'json',
            dataFilter: function(data, type) {
                if (type != 'json' && type != 'jsonp') {
                    return data;
                }
                var prefix = 'for(;;);';
                pos = data.indexOf(prefix);
                if (pos === 0) {
                    return data.substring(prefix.length);
                }
                return data;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if(wykop.nextPageLoading){
                    paginationCopy();
                }
                if (flag != "") {
                    wykop.unsetFlag(flag);
                }
                if (isForm) {
                    $this.find(".submit i").hide();
                    $this.find(".submit").removeAttr("disabled");
                }
            },
            success: function(r, textStatus, jqXHR) {
                if (flag != "") {
                    wykop.unsetFlag(flag);
                }
                if (isForm) {
                    $this.find(".submit i").hide();
                    $this.find(".submit").removeAttr("disabled");
                }
                if (r.error) {
                    if (event)
                        alert(r.error.message);
                    return;
                }
                if (r.operations) {
                    for (var i in r.operations) {
                        var $el = $this;
                        if (r.operations[i].selector) {
                            $el = $(r.operations[i].selector);
                        }
                        if (r.operations[i].closest) {
                            $el = $el.closest(r.operations[i].closest);
                        }
                        if (r.operations[i].find) {
                            $el = $el.find(r.operations[i].find);
                        }
                        switch (r.operations[i].type) {
                            case 'replace':
                                $el.replaceWith(r.operations[i].html);
                                wykop.bindLazy();
                                break;
                            case 'storeAndReplace':
                                var $obj = $(r.operations[i].html);
                                $obj.data('original', $el);
                                $el.replaceWith($obj);
                                wykop.bindLazy();
                                break;
                            case 'append':
                                $el.append(r.operations[i].html);
                                wykop.bindLazy();
                                break;
                            case 'prepend':
                                $el.prepend(r.operations[i].html);
                                wykop.bindLazy();
                                break;
                            case 'before':
                                $el.prepend(r.operations[i].html);
                                wykop.bindLazy();
                                break;
                            case 'content':
                                $el.html(r.operations[i].html);
                                wykop.bindLazy();
                                break;
                            case 'addClass':
                                $el.addClass(r.operations[i].html);
                                break;
                            case 'removeClass':
                                $el.removeClass(r.operations[i].html);
                                break;
                            case 'remove':
                                $el.remove();
                                break;
                            case 'slideUp':
                                $el.slideUp();
                                break;
                            case 'slideDown':
                                $el.slideDown();
                                break;
                            case 'hide':
                                $el.hide();
                                break;
                            case 'show':
                                $el.show();
                                break;
                            case 'focus':
                                $el.focus();
                                break;
                            case 'callback':
                                var fn = wykop[r.operations[i].method];
                                if (typeof fn === 'function') {
                                    fn($el, r.operations[i].data);
                                }
                                break;
                        }
                    }
                }
                paginationCopy();
            }
        });
    };
    function paginationCopy(){
        var footerHeight = 200;
        if ($(window).scrollTop() >= wykop.height.document - wykop.height.window - footerHeight) {
            wykop.nextPageLoading = true;
            paginationButton.hide();
            paginationButton.trigger('click');
            $('#paginationLoader').show();
        }
    }
}
